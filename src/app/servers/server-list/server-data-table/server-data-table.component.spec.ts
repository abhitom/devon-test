import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerDataTableComponent } from './server-data-table.component';

describe('ServerDataTableComponent', () => {
  let component: ServerDataTableComponent;
  let fixture: ComponentFixture<ServerDataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerDataTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
