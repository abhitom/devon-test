import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { ServerList } from '@models/server-list.model';


@Component({
  selector: 'app-server-data-table',
  templateUrl: './server-data-table.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServerDataTableComponent {

  @Input() serverListInput: ServerList;

  errorMessage = 'No servers found';

  trackByFn(index, server) {
    return server.model;
  }

}
