import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { ServerListService } from './server-list.service';

describe('ServerListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServerListService],
      imports: [HttpClientModule]
    });
  });

  it('should be created', inject([ServerListService], (service: ServerListService) => {
    expect(service).toBeTruthy();
  }));
});
