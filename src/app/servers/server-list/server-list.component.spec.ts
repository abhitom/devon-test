import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { ServerListComponent } from './server-list.component';
import { HardDiskTypesFilterComponent } from './server-data-filter/hard-disk-types-filter/hard-disk-types-filter.component';
import { LocationFilterComponent } from './server-data-filter/location-filter/location-filter.component';
import { MinMaxStorageFilterComponent } from './server-data-filter/min-max-storage-filter/min-max-storage-filter.component';
import { RamSizeFilterComponent } from './server-data-filter/ram-size-filter/ram-size-filter.component';
import { ServerListService } from './server-list.service';
import { ServerDataFilterComponent } from './server-data-filter/server-data-filter.component';
import { ServerDataTableComponent } from './server-data-table/server-data-table.component';
import { ProgressBarComponent } from '@shared/progress-bar/progress-bar.component';
import { ServerList } from '@models/server-list.model';
import { GB2TBPipe } from '@shared/gb2tb-pipe/gb2-tb.pipe';
import { Observable, of as observableOf } from 'rxjs';

let serverListServiceStub: Partial<ServerListService>;

describe('ServerListComponent', () => {

  describe('Integration - ServerListComponent', () => {

    let component: ServerListComponent;
    let fixture: ComponentFixture<ServerListComponent>;

    beforeEach(async(() => {
      setUserServiceStub();
      TestBed.configureTestingModule({
        declarations: [GB2TBPipe, ServerListComponent,
          ServerDataFilterComponent, ServerDataTableComponent,
          ProgressBarComponent, HardDiskTypesFilterComponent,
          LocationFilterComponent, MinMaxStorageFilterComponent,
          RamSizeFilterComponent],
        imports: [FormsModule],
        providers: [{ provide: ServerListService, useValue: serverListServiceStub }]
      })
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(ServerListComponent);
      component = fixture.componentInstance;
      component.serverLocations = new Set<string>(['Bangalore']);
      component.serverListObservable = observableOf({ servers: [] });
      component.fetching = false;
    });

    it('should create ServerListComponent and child', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Unit Test - ServerListComponent', () => {

    let component: ServerListComponent;
    let fixture: ComponentFixture<ServerListComponent>;

    beforeEach(async(() => {
      setUserServiceStub();
      TestBed.configureTestingModule({
        declarations: [ServerListComponent],
        imports: [FormsModule],
        providers: [{ provide: ServerListService, useValue: serverListServiceStub }],
        schemas: [NO_ERRORS_SCHEMA]
      })
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(ServerListComponent);
      component = fixture.componentInstance;
    });

    it('should create ServerListComponent', () => {
      expect(component).toBeTruthy();
    });

  });
});





const setUserServiceStub = () => {
  serverListServiceStub = {
    getServers(): Observable<ServerList> {

      return observableOf({
        'servers': [{
          'model': 'Dell R210Intel Xeon X3440',
          'ram': {
            'memory': '16',
            'unit': 'GB',
            'type': 'DDR3'
          },
          'hdd': {
            'memory': '2',
            'count': '2',
            'unit': 'TB',
            'type': 'SATA2'
          },
          'location': 'AmsterdamAMS-01',
          'price': {
            'currency': 'EUR',
            'currencySymbol': '€',
            'amountCents': 4999
          }
        }]
      });
    }

  };
};
