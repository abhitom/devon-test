import { Component, ChangeDetectionStrategy, OnInit, Input, AfterViewInit, Output, EventEmitter, ViewChild } from '@angular/core';

import { DataFilterModel } from '@models/server-data-filter.model';
import { HardDiskTypesFilterComponent } from './hard-disk-types-filter/hard-disk-types-filter.component';
import { LocationFilterComponent } from './location-filter/location-filter.component';
import { RamSizeFilterComponent } from './ram-size-filter/ram-size-filter.component';
import { MinMaxStorageFilterComponent } from './min-max-storage-filter/min-max-storage-filter.component';

@Component({
  selector: 'app-server-data-filter',
  templateUrl: './server-data-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServerDataFilterComponent implements OnInit, AfterViewInit {

  @ViewChild(HardDiskTypesFilterComponent)
  private hddTypesFilterChildComponent: HardDiskTypesFilterComponent;

  @ViewChild(LocationFilterComponent)
  private locationFilterChildComponent: LocationFilterComponent;

  @ViewChild(RamSizeFilterComponent)
  private ramSizeFilterChildComponent: RamSizeFilterComponent;

  @ViewChild(MinMaxStorageFilterComponent)
  private minMaxStorageFilterChildComponent: MinMaxStorageFilterComponent;

  @Input() locationInput: Array<string>;
  @Input() disableButtons: boolean;
  @Output() filter: EventEmitter<DataFilterModel> = new EventEmitter<DataFilterModel>();


  dataFilterStore: DataFilterModel;

  ngOnInit() {
    this.dataFilterStore = new DataFilterModel(0);
  }

  ngAfterViewInit() {

    this.initDataStore();

  }

  private initDataStore() {

    const maxStorageIndex: number = this.minMaxStorageFilterChildComponent.selectedMaxStorageIndex;
    const maxStorageSize: number = this.minMaxStorageFilterChildComponent.storageSizesInGB[maxStorageIndex];
    this.dataFilterStore = new DataFilterModel(maxStorageSize);
  }

  onHardDiskTypeChange(type: string) {

    this.dataFilterStore.selectedHardDiskType = type;
    // console.log(this.dataFilterStore);

  }

  onLocationFilterChange(location: string) {

    this.dataFilterStore.selectedLocation = location;
    // console.log(this.dataFilterStore);

  }
  onRamFilterChange(ramSize: any) {
    const has: boolean = this.dataFilterStore.selectedRamSizes.has(ramSize.size);
    if (!has && ramSize.checked) {
      this.dataFilterStore.selectedRamSizes.add(ramSize.size);
    } else {
      this.dataFilterStore.selectedRamSizes.delete(ramSize.size);
    }
  }

  onSelectedMinStorageChange(minValue) {
    // console.log('setting min to dataStore', minValue);
    this.dataFilterStore.selectedMinStorageSize = minValue;
  }
  onSelectedMaxStorageChange(maxValue) {
    // console.log('setting max to dataStore', maxValue);
    this.dataFilterStore.selectedMaxStorageSize = maxValue;
  }
  private emitFilterEvent() {
    this.filter.emit(this.dataFilterStore);
  }
  onFilter() {
    this.emitFilterEvent();
  }

  onReset() {
    this.dataFilterStore.reset();
    this.minMaxStorageFilterChildComponent.reset();
    this.ramSizeFilterChildComponent.reset();
    this.hddTypesFilterChildComponent.reset();
    this.locationFilterChildComponent.reset();
    this.emitFilterEvent();
  }
}
