import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { DataFilterModel } from '@models/server-data-filter.model';

import { ServerDataFilterComponent } from './server-data-filter.component';
import { HardDiskTypesFilterComponent } from './hard-disk-types-filter/hard-disk-types-filter.component';
import { LocationFilterComponent } from './location-filter/location-filter.component';
import { MinMaxStorageFilterComponent } from './min-max-storage-filter/min-max-storage-filter.component';
import { RamSizeFilterComponent } from './ram-size-filter/ram-size-filter.component';

import { GB2TBPipe } from '@shared/gb2tb-pipe/gb2-tb.pipe';


describe('ServerDataFilterComponent', () => {
  let component: ServerDataFilterComponent;
  let fixture: ComponentFixture<ServerDataFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServerDataFilterComponent, HardDiskTypesFilterComponent,
        LocationFilterComponent, MinMaxStorageFilterComponent,
        RamSizeFilterComponent, GB2TBPipe],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerDataFilterComponent);
    component = fixture.componentInstance;
    component.disableButtons = false;
    component.dataFilterStore = null;
    component.locationInput = [];
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should insert locations to location dropdown', () => {
    component.locationInput.push('Bangalore', 'Mysore', 'Hyderabad');
    fixture.detectChanges();

    const compiled: HTMLElement = fixture.debugElement.nativeElement;
    const selectTag: HTMLSelectElement = compiled.querySelector('#location');
    expect(selectTag.options.item(0).value).toEqual('');
    expect(selectTag.options.item(1).value).toEqual('Bangalore');
    expect(selectTag.options.item(2).value).toEqual('Mysore');
    expect(selectTag.options.item(3).value).toEqual('Hyderabad');
  });

  it('should insert hard disk types to hard disk types dropdown', () => {
    // component.hardDiskTypes.push('SAS', 'SATA');
    fixture.detectChanges();

    const compiled: HTMLElement = fixture.debugElement.nativeElement;
    const selectTag: HTMLSelectElement = compiled.querySelector('#hardDisk');
    expect(selectTag.options.item(0).value).toEqual('');
    expect(selectTag.options.item(1).value).toEqual('SAS');
    expect(selectTag.options.item(2).value).toEqual('SATA2');
  });

  it('should enable and disable buttons', () => {

    const compiled: HTMLElement = fixture.debugElement.nativeElement;
    const filterBtn: HTMLButtonElement = compiled.querySelector('#filter-btn');
    const clearBtn: HTMLButtonElement = compiled.querySelector('#clear-btn');
    component.dataFilterStore = new DataFilterModel(7200);

    fixture.detectChanges();

    expect(filterBtn.disabled).toEqual(true);
    expect(clearBtn.disabled).toEqual(true);


  });
});
