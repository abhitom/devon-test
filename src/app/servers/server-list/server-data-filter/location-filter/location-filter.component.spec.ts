import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { LocationFilterComponent } from './location-filter.component';
import { GB2TBPipe } from '@shared/gb2tb-pipe/gb2-tb.pipe';

describe('LocationFilterComponent', () => {
  let component: LocationFilterComponent;
  let fixture: ComponentFixture<LocationFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LocationFilterComponent, GB2TBPipe],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationFilterComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add drop downs', () => {
    component.locationInput = [];
    component.locationInput.push('Bangalore', 'Mysore', 'Hyderabad');
    fixture.detectChanges();

    const compiled: HTMLElement = fixture.debugElement.nativeElement;
    const selectTag: HTMLSelectElement = compiled.querySelector('#location');
    expect(selectTag.options.item(0).value).toEqual('');
    expect(selectTag.options.item(1).value).toEqual('Bangalore');
    expect(selectTag.options.item(2).value).toEqual('Mysore');
    expect(selectTag.options.item(3).value).toEqual('Hyderabad');


  });
});
