import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-location-filter',
  templateUrl: './location-filter.component.html'
})
export class LocationFilterComponent {

  @Input() locationInput: Array<string>;
  @Output() locationChange: EventEmitter<string> = new EventEmitter<string>();

  selectedLocation = '';

  onChange() {

    this.locationChange.emit(this.selectedLocation);

  }

  reset() {
    this.selectedLocation = '';
  }
}
