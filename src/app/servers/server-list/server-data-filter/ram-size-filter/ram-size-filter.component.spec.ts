import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { RamSizeFilterComponent } from './ram-size-filter.component';
import { GB2TBPipe } from '@shared/gb2tb-pipe/gb2-tb.pipe';

describe('RamSizeFilterComponent', () => {
  let component: RamSizeFilterComponent;
  let fixture: ComponentFixture<RamSizeFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RamSizeFilterComponent, GB2TBPipe],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RamSizeFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
