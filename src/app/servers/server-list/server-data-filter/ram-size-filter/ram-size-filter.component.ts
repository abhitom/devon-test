import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnInit } from '@angular/core';



const ramSizes: Array<number> = [2, 4, 8, 12, 16, 24, 48, 64, 96];

@Component({
  selector: 'app-ram-size-filter',
  templateUrl: './ram-size-filter.component.html'
})
export class RamSizeFilterComponent implements OnChanges, OnInit {

  @Input() ramSizesInGB: Set<number> = new Set<number>(ramSizes);
  @Output() ramFilterChange: EventEmitter<any> = new EventEmitter<any>();

  selectedRamSizes: Array<any> = [];

  ngOnInit() {
    this.ramSizesInGB.forEach((size_) => {
      this.selectedRamSizes.push({ size: size_, checked: false });
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['ramSizesInGB'] && changes['ramSizesInGB'].previousValue !== changes['ramSizesInGB'].currentValue) {
     // console.log('ramSize input changed');
      this.selectedRamSizes = [];
      this.ramSizesInGB.forEach((size_) => {
        this.selectedRamSizes.push({ size: size_, checked: false });
      });
    }
  }

  onRamSizeChange(ramSize: any) {
    this.ramFilterChange.emit(ramSize);
  }

  reset() {
    if (this.selectedRamSizes.length > 0) {
      this.selectedRamSizes.forEach((ramSize) => {
        ramSize.checked = false;
      });
    }
  }
}
