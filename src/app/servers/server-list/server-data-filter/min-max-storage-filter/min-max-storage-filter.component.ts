import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

const defaultStorageSizes: Array<number> = [0, 250, 500, 1000, 2000, 3000, 4000, 8000, 12000, 24000, 48000, 72000];

@Component({
  selector: 'app-min-max-storage-filter',
  templateUrl: './min-max-storage-filter.component.html'
})
export class MinMaxStorageFilterComponent implements OnInit {

  @Input() storageSizesInGB: Array<number> = defaultStorageSizes;
  @Output() minChange: EventEmitter<number> = new EventEmitter<number>();
  @Output() maxChange: EventEmitter<number> = new EventEmitter<number>();

  selectedMinStorageIndex = 0;
  selectedMaxStorageIndex = 0;

  ngOnInit() {
    this.selectedMaxStorageIndex = this.storageSizesInGB.length - 1;
  }

  onSelectedMinChange(e) {
    this.minChange.emit(this.storageSizesInGB[this.selectedMinStorageIndex]);
  }

  onSelectedMaxChange() {
    this.maxChange.emit(this.storageSizesInGB[this.selectedMaxStorageIndex]);
  }

  reset() {
    this.selectedMinStorageIndex = 0;
    this.selectedMaxStorageIndex = this.storageSizesInGB.length - 1;
  }

}
