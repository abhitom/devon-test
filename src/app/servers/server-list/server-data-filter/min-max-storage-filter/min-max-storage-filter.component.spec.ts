import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { MinMaxStorageFilterComponent } from './min-max-storage-filter.component';
import { GB2TBPipe } from '@shared/gb2tb-pipe/gb2-tb.pipe';

describe('MinMaxStorageFilterComponent', () => {
  let component: MinMaxStorageFilterComponent;
  let fixture: ComponentFixture<MinMaxStorageFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinMaxStorageFilterComponent, GB2TBPipe ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinMaxStorageFilterComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
