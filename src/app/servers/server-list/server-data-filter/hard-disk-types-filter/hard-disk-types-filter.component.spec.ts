import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { HardDiskTypesFilterComponent } from './hard-disk-types-filter.component';
import { GB2TBPipe } from '@shared/gb2tb-pipe/gb2-tb.pipe';

describe('HardDiskTypesFilterComponent', () => {
  let component: HardDiskTypesFilterComponent;
  let fixture: ComponentFixture<HardDiskTypesFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HardDiskTypesFilterComponent, GB2TBPipe ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HardDiskTypesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add drop downs', () => {

    fixture.detectChanges();

    const compiled: HTMLElement = fixture.debugElement.nativeElement;
    const selectTag: HTMLSelectElement = compiled.querySelector('#hardDisk');
    expect(selectTag.options.item(0).value).toEqual('');
    expect(selectTag.options.item(1).value).toEqual('SAS');
    expect(selectTag.options.item(2).value).toEqual('SATA2');


  });
});
