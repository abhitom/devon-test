import { Component, Input, Output, EventEmitter } from '@angular/core';


const hardDiskTypes: Array<string> = ['SAS', 'SATA2', 'SSD'];

@Component({
  selector: 'app-hard-disk-types-filter',
  templateUrl: './hard-disk-types-filter.component.html'
})
export class HardDiskTypesFilterComponent {

  @Input() hardDiskTypes: Array<string> = hardDiskTypes;
  @Output() typeChange: EventEmitter<string> = new EventEmitter<string>();

  selectedHardDiskType = '';


  onChange() {

    this.typeChange.emit(this.selectedHardDiskType);

  }

  reset() {
    this.selectedHardDiskType = '';
  }

}
