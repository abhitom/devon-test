import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { tap, finalize } from 'rxjs/operators';
import { ServerList } from '@models/server-list.model';
import { ServerListService } from './server-list.service';
import { DataFilterModel } from '@models/server-data-filter.model';

@Component({
  selector: 'app-server-list',
  templateUrl: './server-list.component.html'
})
export class ServerListComponent implements OnInit {

  constructor(private serversService: ServerListService) {

  }

  fetching = false;
  serverLocations: Set<string> = new Set<string>();
  serverListObservable: Observable<ServerList>;

  ngOnInit() {

    this.getServersFromService();
  }

  onFilter(filter: DataFilterModel) {
    this.getServersFromService(filter);
  }

  private getServersFromService(filter?: DataFilterModel) {

    if (this.fetching) { return; }

    this.fetching = true;

    this.serverListObservable = this.serversService.getServers(filter)
      .pipe(tap((serverList: ServerList) => {
        if (this.serverLocations.size === 0) {
          serverList.servers.map((e) => e.location).forEach((loc) => {
            this.serverLocations.add(loc);
          });
        }
      }))
      .pipe(finalize(() => {
        this.fetching = false;
      }));

  }

}
