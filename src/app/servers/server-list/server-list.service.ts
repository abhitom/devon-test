import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { ServerList } from '@models/server-list.model';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DataFilterModel } from '@models/server-data-filter.model';

// const url = '/assets/stub/servers.json';
const url = 'http://85.17.31.99:4300/api/servers';

@Injectable()
export class ServerListService {

  constructor(private http: HttpClient) { }

  getServers(filters?: DataFilterModel): Observable<ServerList> {

    return this.http.get<ServerList>(url, { params: this.getQueryParams(filters) }).pipe(
      catchError(this.handleError())
    );

  }

  private handleError() {
    return (error: any): Observable<ServerList> => {
      console.error(error);
      return of({ servers: [] });
    };
  }

  private getQueryParams(filters: DataFilterModel): HttpParams {

    let params: HttpParams = new HttpParams();

    if (filters && filters.hasFilters()) {
      if (filters.hasStorageFilters()) {
        params = params.append('storageMin', filters.selectedMinStorageSize.toString());
        params = params.append('storageMax', filters.selectedMaxStorageSize.toString());
      }
      if (filters.hasRamFilters()) {
        params = params.append('ram', Array.from(filters.selectedRamSizes).join(','));
      }
      if (filters.hasHardDiskTypeFilters()) {
        params = params.append('hdd', filters.selectedHardDiskType);
      }
      if (filters.hasLocationFilters()) {
        params = params.append('location', filters.selectedLocation);
      }
    }

    return params;
  }

}
