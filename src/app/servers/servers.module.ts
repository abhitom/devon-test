import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ServerListComponent } from './server-list/server-list.component';
import { ServerDataFilterComponent } from './server-list/server-data-filter/server-data-filter.component';
import { ServerDataTableComponent } from './server-list/server-data-table/server-data-table.component';
import { ServersComponent } from './servers.component';
import { ServerListService } from './server-list/server-list.service';
import { SharedModule } from '@shared/shared.module';
import { HardDiskTypesFilterComponent } from './server-list/server-data-filter/hard-disk-types-filter/hard-disk-types-filter.component';
import { LocationFilterComponent } from './server-list/server-data-filter/location-filter/location-filter.component';
import { RamSizeFilterComponent } from './server-list/server-data-filter/ram-size-filter/ram-size-filter.component';
import { MinMaxStorageFilterComponent } from './server-list/server-data-filter/min-max-storage-filter/min-max-storage-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    SharedModule
  ],
  exports: [
    ServersComponent
  ],
  declarations: [ServersComponent, ServerListComponent,
    ServerDataFilterComponent, ServerDataTableComponent,
    HardDiskTypesFilterComponent, LocationFilterComponent,
    RamSizeFilterComponent, MinMaxStorageFilterComponent],
  providers: [ServerListService]
})
export class ServersModule { }
