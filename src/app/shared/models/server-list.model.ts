export interface ServerList {
  servers: Server[];
}

interface Server {
  model: string;
  ram: RAM;
  hdd: Hdd;
  location: string;
  price: Price;
}

interface Price {
  currency: string;
  currencySymbol: string;
  amountCents: number;
}

interface Hdd {
  memory: string;
  count: string;
  unit: string;
  type: string;
}

interface RAM {
  memory: string;
  unit: string;
  type: string;
}
