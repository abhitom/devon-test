export class DataFilterModel {
  private selectedStorageSize_: StorageRange;
  private selectedHardDiskType_ = '';
  private selectedLocation_ = '';
  private selectedRamSizes_: Set<number> = new Set<number>();

  constructor(maxStorage: number) {
    this.selectedStorageSize_ = new StorageRange(maxStorage);
  }
  public hasFilters(): boolean {
    return this.hasStorageFilters() ||
      this.hasHardDiskTypeFilters() ||
      this.hasLocationFilters() ||
      this.hasRamFilters();
  }
  public reset() {
    this.selectedStorageSize_.reset();
    this.selectedHardDiskType_ = '';
    this.selectedLocation_ = '';
    this.selectedRamSizes_.clear();
  }
  public hasStorageFilters(): boolean {
    return this.selectedStorageSize_.hasRange();
  }

  public hasHardDiskTypeFilters(): boolean {
    return this.selectedHardDiskType_ !== '';
  }

  public hasLocationFilters(): boolean {
    return this.selectedLocation_ !== '';
  }

  public hasRamFilters(): boolean {
    return this.selectedRamSizes_.size !== 0;
  }

  public get selectedMinStorageSize(): number {
    return this.selectedStorageSize_.min;
  }

  public set selectedMinStorageSize(value: number) {
    this.selectedStorageSize_.min = value;
  }

  public get selectedMaxStorageSize(): number {
    return this.selectedStorageSize_.max;
  }

  public set selectedMaxStorageSize(value: number) {
    this.selectedStorageSize_.max = value;
  }

  public get selectedHardDiskType() {
    return this.selectedHardDiskType_;
  }

  public set selectedHardDiskType(value: string) {
    this.selectedHardDiskType_ = value;
  }
  public get selectedLocation() {
    return this.selectedLocation_;
  }

  public set selectedLocation(value: string) {
    this.selectedLocation_ = value;
  }
  public get selectedRamSizes() {
    return this.selectedRamSizes_;
  }

  public set selectedRamSizes(value: Set<number>) {
    this.selectedRamSizes_ = value;
  }

}

class StorageRange {
  public min = 0;
  public max = 0;
  private initialMax = 0;
  constructor(initialMax: number) {
    this.max = this.initialMax = initialMax;
  }
  hasRange(): boolean {
    return this.min !== 0 || this.max !== this.initialMax;
  }
  reset() {
    this.min = 0;
    this.max = this.initialMax;
  }
}
