import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { GB2TBPipe } from './gb2tb-pipe/gb2-tb.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ProgressBarComponent, GB2TBPipe],
  exports: [ProgressBarComponent, GB2TBPipe]
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule
    };
  }
}
