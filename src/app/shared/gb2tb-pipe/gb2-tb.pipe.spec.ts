import { GB2TBPipe } from './gb2-tb.pipe';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

@Component({
  template: '<div> {{ value | GB2TB }}</div>'
})
export class GB2TBPipeHostComponent {
  value: number;
}

describe('GB2TBPipe', () => {
  const pipe = new GB2TBPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  describe('Bad Inputs', () => {

    it('should return the object', () => {
      const actual = pipe.transform({ a: 'a' } as any);
      expect(actual).toEqual({ a: 'a' } as any);
    });

    it('should return null ', () => {
      expect(pipe.transform(null)).toEqual(null);
    });

    it('should return undefined', () => {
      expect(pipe.transform(undefined)).toEqual(undefined);
    });

    it('should return value if NaN', () => {
      expect(pipe.transform(NaN)).toEqual(NaN);
      expect(pipe.transform('xyz')).toEqual('xyz');
    });

  });

  describe('GB2TB pipe inside a Component', () => {
    beforeEach(async(() => {
      TestBed
        .configureTestingModule({
          declarations: [GB2TBPipe, GB2TBPipeHostComponent]
        })
        .compileComponents();
    }));

    let fixture: ComponentFixture<GB2TBPipeHostComponent>;
    let debugElement: DebugElement;

    let component: GB2TBPipeHostComponent;

    beforeEach(() => {
      fixture = TestBed.createComponent(GB2TBPipeHostComponent);
      debugElement = fixture.debugElement;
      component = fixture.componentInstance;
    });

    it('should create an instance', () => {
      expect(fixture).toBeTruthy();
    });

    it('should display 1 GB', () => {
      component.value = 1;
      fixture.detectChanges();

      const div: HTMLDivElement = debugElement
        .query(By.css('div'))
        .nativeElement;

      expect(div.textContent.trim()).toEqual('1 GB');
    });

    it('should display 0 GB', () => {
      component.value = 0;
      fixture.detectChanges();

      const div: HTMLDivElement = debugElement
        .query(By.css('div'))
        .nativeElement;

      expect(div.textContent.trim()).toEqual('0 GB');
    });

    it('should display 2 TB', () => {
      component.value = 2000;
      fixture.detectChanges();

      const div: HTMLDivElement = debugElement
        .query(By.css('div'))
        .nativeElement;

      expect(div.textContent.trim()).toEqual('2 TB');
    });
  });
});



