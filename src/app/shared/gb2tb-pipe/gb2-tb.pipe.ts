import { Pipe, PipeTransform } from '@angular/core';

const factor = 1000;
@Pipe({
  name: 'GB2TB'
})
export class GB2TBPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === null || isNaN(value)) {
      return value;
    } else if (value >= 1000) {
      return  `${value / 1000} TB`;
    } else {
      return  `${value} GB`;
    }
  }

}
